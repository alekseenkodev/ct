@extends('layouts.app')
@section('content')

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Coalition technologies laravel test</a>
            </div>


        </div><!-- /.container-fluid -->
    </nav>


    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">

                <div class="bs-example" data-example-id="basic-forms">
                    <form action="#" method="post" id="product_form">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Product name</label>
                            <input type="text" class="form-control" id="product_name" placeholder="Enter product name">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Quantity in stock</label>
                            <input type="number" class="form-control" id="quantity"
                                   placeholder="Enter quantity">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Price per item</label>
                            <input type="number" class="form-control" id="price"
                                   placeholder="Enter price per item">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>


            </div>
            <div class="col-md-2"></div>

            <div class="row">

                <table class="table table-stripped table-bordered" id="product_blade">
                    <thead>
                    <th>Product</th>
                    <th>Quantity in stock</th>
                    <th>Price per item</th>
                    <th>Datetime submitted</th>
                    <th>Total value number</th>
                    </thead>
                    <tbody>
                    <?php $sum_tot_Price = 0 ?>
                    @if(sizeof($products) > 0)
                        @foreach($products as $product)

                            <tr>
                                <td>
                                    {{ $product->product_name }}

                                </td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->date_add }}</td>
                                <td>{{ $product->total }}</td>
                            </tr>
                            <?php $sum_tot_Price += $product->total ?>
                        @endforeach
                    @endif
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>

                        </td>
                        <td>Total: <span id="total">{{ $sum_tot_Price}}</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </div>


@endsection