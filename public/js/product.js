// $(document).ready(function () {
//
// });

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#product_form").submit(function (event) {
    var product_name = $("#product_name").val();
    var quantity = $("#quantity").val();
    var price = $("#price").val();
    var current_total = $("#total").html();

    $.ajax({
        type: 'POST',
        url: '/save_data',
        data: {
            product_name: product_name,
            quantity: quantity,
            price: price,

        },
        dataType: 'json',
        success: function (answer) {
            if (answer.status == 'ok') {

                $("#product_blade tbody tr:first-child").before(
                    '<tr>' +
                    '<td>' + product_name + '</td>' +
                    '<td>' + quantity + '</td>' +
                    '<td>' + price + '</td>' +
                    '<td>' + answer.params.date_add + '</td>' +
                    '<td>'+quantity*price+'</td>'+
                    '</tr>'
                );


                $("#total").html(parseInt(current_total)+(quantity*price));


            }
        }
    })

    event.preventDefault();

});