<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    //


    public function index(Request $request)
    {


        return view('index', ['products' => json_decode(file_get_contents('../public/data.json'))]);
    }


    public function saveData(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'string|required',
            'quantity' => 'integer|required',
            'price' => 'numeric|required'
        ]);

        $params = [
            'product_name' => $request->product_name,
            'quantity' => $request->quantity,
            'price' => $request->price,
            'date_add' => date("Y-m-d H:i:s"),
            'total' => $request->price*$request->quantity
        ];


        $current_data  = json_decode(file_get_contents('../public/data.json'));
         if(sizeof($current_data) > 0){
             array_push($current_data, $params);
         }else{
             $current_data = $params;
         }


        header('Content-Type: application/json; charset=utf-8');
        $current_data = file_put_contents('../public/data.json', json_encode($current_data, JSON_UNESCAPED_UNICODE));

        return ['status' => 'ok','params' => $params];

    }


}
